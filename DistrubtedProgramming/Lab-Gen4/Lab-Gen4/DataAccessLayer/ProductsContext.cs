﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Lab_Gen4.Models;

namespace Lab_Gen4.DataAccessLayer
{
    public class ProductsContext : DbContext
    {
        public ProductsContext() : base("ProductsContext") { }

        public DbSet<Category> Categories {get; set;}
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}