﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PayPal.Api;
using OnlineShop.Models;
using System.Web.Http.Cors;
using OnlineShop.DataAccess;
namespace OnlineShopApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PaypalController : ApiController
    {
        [HttpPost]
        //Get payment paypal url
        public string GetPaymentPayPalURL(PayPalObjectInfo paypalObject)
        {
            string paypalURL = "";
            double cartAmount = 0;
            var itemList = new ItemList();
            var items = new List<Item>();
            dynamic products = paypalObject.ProductList;
            //get the paypal api context
            var apiContext = PayPalConfiguration.GetAPIContext();
            //ProductController product = new ProductController();
            ProductsRepository productsRepo = new ProductsRepository();
            //map into paypal items list and get the total amount
            foreach (var cartItem in products)
            {
                var prod = productsRepo.GetProduct(Convert.ToInt32(cartItem.Name));
                if (cartItem.Value > 0)
                {
                  
                    var Item = new Item();
                    Item.name = prod.ProductName;
                    Item.currency = paypalObject.Currency;
                    Item.price = Math.Round(prod.ProductPrice, 2).ToString();
                    Item.quantity = cartItem.Value;
                    items.Add(Item);
                    cartAmount += Convert.ToDouble(Math.Round(prod.ProductPrice , 2)) * Convert.ToDouble(Item.quantity);
                }
            }

            itemList.items = items;
            cartAmount = Math.Round(cartAmount, 2);

            var payer = new Payer() { payment_method = "paypal" };
            var redirUrls = new RedirectUrls()
            {
                cancel_url = paypalObject.SiteURL + "?cancel=true",
                return_url = paypalObject.SiteURL
            };

            var details = new Details()
            {
                tax = paypalObject.Tax.ToString(),
                shipping = paypalObject.ShippingFee.ToString(),
                subtotal = cartAmount.ToString()
            };

            var paypalAmount = new Amount() { currency = paypalObject.Currency, total = cartAmount.ToString(), details = details };

            var transactionList = new List<PayPal.Api.Transaction>();
            PayPal.Api.Transaction transaction = new PayPal.Api.Transaction();
            transaction.description = paypalObject.OrderDescription;
            transaction.invoice_number = paypalObject.InvoiceNumber;
            transaction.amount = paypalAmount;
            transaction.item_list = itemList;
            transactionList.Add(transaction);

            var payment = new Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };

            try
            {
                var createdPayment = payment.Create(apiContext);
                var links = createdPayment.links.GetEnumerator();
                while (links.MoveNext())
                {
                    var link = links.Current;
                    if (link.rel.ToLower().Trim().Equals("approval_url"))
                    {
                        paypalURL = link.href;
                    }
                }

            }
            catch (PayPal.PaymentsException ex)
            {
                paypalURL = "ERROR: " + ex.Response;
            }

            return paypalURL;
        }

        [HttpPost]
        //Payment Processing
        public Payment DoPayment(PayPalObjectInfo objPayPalObject)
        {
            var apiContext = PayPalConfiguration.GetAPIContext();
            var paymentExecution = new PaymentExecution() { payer_id = objPayPalObject.PayerID };
            var payment = new Payment() { id = objPayPalObject.PaymentID };
            var executedPayment = new Payment();
            try
            {
                executedPayment = payment.Execute(apiContext, paymentExecution);
            }
            catch (PayPal.PaymentsException ex)
            {
                throw new Exception("Sorry there is an error processing the payment. " + ex.Response);
            }

            return executedPayment;
        }

        public class PayPalObjectInfo
        {
            public Object ProductList { get; set; }
            public string SiteURL { get; set; }
            public string InvoiceNumber { get; set; }
            public string Currency { get; set; }
            public string Tax { get; set; }
            public string ShippingFee { get; set; }
            public string OrderDescription { get; set; }

            //Used for DoPayment
            public string PayerID { get; set; }
            public string PaymentID { get; set; }
        }
    }
}
