﻿namespace Consuming_Calc_Service
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Number1 = new System.Windows.Forms.Label();
            this.lbl_Number2 = new System.Windows.Forms.Label();
            this.txt_Number1 = new System.Windows.Forms.TextBox();
            this.txt_Number2 = new System.Windows.Forms.TextBox();
            this.btn_Add = new System.Windows.Forms.Button();
            this.btn_Subtract = new System.Windows.Forms.Button();
            this.btn_Multiply = new System.Windows.Forms.Button();
            this.btn_Divide = new System.Windows.Forms.Button();
            this.lbl_Result = new System.Windows.Forms.Label();
            this.lbl_ResultValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_Number1
            // 
            this.lbl_Number1.AutoSize = true;
            this.lbl_Number1.Location = new System.Drawing.Point(30, 40);
            this.lbl_Number1.Name = "lbl_Number1";
            this.lbl_Number1.Size = new System.Drawing.Size(53, 13);
            this.lbl_Number1.TabIndex = 0;
            this.lbl_Number1.Text = "Number 1";
            // 
            // lbl_Number2
            // 
            this.lbl_Number2.AutoSize = true;
            this.lbl_Number2.Location = new System.Drawing.Point(30, 82);
            this.lbl_Number2.Name = "lbl_Number2";
            this.lbl_Number2.Size = new System.Drawing.Size(53, 13);
            this.lbl_Number2.TabIndex = 1;
            this.lbl_Number2.Text = "Number 2";
            // 
            // txt_Number1
            // 
            this.txt_Number1.Location = new System.Drawing.Point(89, 37);
            this.txt_Number1.Name = "txt_Number1";
            this.txt_Number1.Size = new System.Drawing.Size(100, 20);
            this.txt_Number1.TabIndex = 2;
            // 
            // txt_Number2
            // 
            this.txt_Number2.Location = new System.Drawing.Point(89, 73);
            this.txt_Number2.Name = "txt_Number2";
            this.txt_Number2.Size = new System.Drawing.Size(100, 20);
            this.txt_Number2.TabIndex = 3;
            // 
            // btn_Add
            // 
            this.btn_Add.Location = new System.Drawing.Point(246, 30);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(75, 23);
            this.btn_Add.TabIndex = 4;
            this.btn_Add.Text = "Add";
            this.btn_Add.UseVisualStyleBackColor = true;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // btn_Subtract
            // 
            this.btn_Subtract.Location = new System.Drawing.Point(246, 73);
            this.btn_Subtract.Name = "btn_Subtract";
            this.btn_Subtract.Size = new System.Drawing.Size(75, 23);
            this.btn_Subtract.TabIndex = 5;
            this.btn_Subtract.Text = "Subtract";
            this.btn_Subtract.UseVisualStyleBackColor = true;
            this.btn_Subtract.Click += new System.EventHandler(this.btn_Subtract_Click);
            // 
            // btn_Multiply
            // 
            this.btn_Multiply.Location = new System.Drawing.Point(246, 118);
            this.btn_Multiply.Name = "btn_Multiply";
            this.btn_Multiply.Size = new System.Drawing.Size(75, 23);
            this.btn_Multiply.TabIndex = 6;
            this.btn_Multiply.Text = "Multiply";
            this.btn_Multiply.UseVisualStyleBackColor = true;
            this.btn_Multiply.Click += new System.EventHandler(this.btn_Multiply_Click);
            // 
            // btn_Divide
            // 
            this.btn_Divide.Location = new System.Drawing.Point(246, 157);
            this.btn_Divide.Name = "btn_Divide";
            this.btn_Divide.Size = new System.Drawing.Size(75, 23);
            this.btn_Divide.TabIndex = 7;
            this.btn_Divide.Text = "Divide";
            this.btn_Divide.UseVisualStyleBackColor = true;
            this.btn_Divide.Click += new System.EventHandler(this.btn_Divide_Click);
            // 
            // lbl_Result
            // 
            this.lbl_Result.AutoSize = true;
            this.lbl_Result.Location = new System.Drawing.Point(30, 144);
            this.lbl_Result.Name = "lbl_Result";
            this.lbl_Result.Size = new System.Drawing.Size(48, 13);
            this.lbl_Result.TabIndex = 8;
            this.lbl_Result.Text = "Results: ";
            // 
            // lbl_ResultValue
            // 
            this.lbl_ResultValue.AutoSize = true;
            this.lbl_ResultValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 21F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ResultValue.Location = new System.Drawing.Point(110, 144);
            this.lbl_ResultValue.Name = "lbl_ResultValue";
            this.lbl_ResultValue.Size = new System.Drawing.Size(31, 32);
            this.lbl_ResultValue.TabIndex = 9;
            this.lbl_ResultValue.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 413);
            this.Controls.Add(this.lbl_ResultValue);
            this.Controls.Add(this.lbl_Result);
            this.Controls.Add(this.btn_Divide);
            this.Controls.Add(this.btn_Multiply);
            this.Controls.Add(this.btn_Subtract);
            this.Controls.Add(this.btn_Add);
            this.Controls.Add(this.txt_Number2);
            this.Controls.Add(this.txt_Number1);
            this.Controls.Add(this.lbl_Number2);
            this.Controls.Add(this.lbl_Number1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Number1;
        private System.Windows.Forms.Label lbl_Number2;
        private System.Windows.Forms.TextBox txt_Number1;
        private System.Windows.Forms.TextBox txt_Number2;
        private System.Windows.Forms.Button btn_Add;
        private System.Windows.Forms.Button btn_Subtract;
        private System.Windows.Forms.Button btn_Multiply;
        private System.Windows.Forms.Button btn_Divide;
        private System.Windows.Forms.Label lbl_Result;
        private System.Windows.Forms.Label lbl_ResultValue;
    }
}

