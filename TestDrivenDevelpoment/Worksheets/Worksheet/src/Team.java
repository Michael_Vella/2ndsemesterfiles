import java.util.ArrayList;
import java.util.List;

public class Team {
	private List<Player> players = new ArrayList<Player>();
	public static int FULL_TEAM = 21;
	/**
	 * Insert a player
	 * @param p
	 */
	public void addPlayer(Player p) {
		if(!teamAtMax()) {
			int number = p.getNumber();
			if(findPlayer(number) == null) {
				players.add(p);
			}
		}
	}
	
	/**
	 * 
	 * @return true if players list at 21 or greater 
	 */
	private boolean teamAtMax() {
		
		return players.size() >= FULL_TEAM;
	}
	
	/**
	 * 
	 * @return True if there is at least 1 GKP and 10 other players in the team
	 */
	public boolean canPlayTeam() {
		int goalKeep = 0;
		int otherPlayers = 0;
		for (Player p : players)
			if(p.getRole() == PlayerRole.GKP) {
				goalKeep++;
			}else {
				otherPlayers++;
			}
		
		return otherPlayers >= 10 && goalKeep >=1;
	}
	
	/**
	 * 
	 * @param num
	 * @return player as null if not found, else player object
	 */
	private Player findPlayer (int num) {
		Player p = null;
			if(players.isEmpty()) {
				return p = null;
			}
			else {
				for (Player search : players) {
					if (search.getNumber() == num) {
						p = search;//if player is found
						return p;
					}
					else {
						return p = null;
					}
				}
			}
			
		return p;
	}
	
	/**
	 * Remove a found player 
	 * @param num
	 */
	public void removePlayer (int num) {
		Player p = findPlayer(num);
		
		if(p !=  null ) {
			players.remove(p);
		}
		else {
			//player doesn't exist
		}
		
	}
	
	/**
	 * 
	 * @return team count 
	 */
	public int teamCount() {
		return players.toArray().length;
	}
	
	
}
