package ws4;

import static org.junit.Assert.assertTrue;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Stopwatch;

import ws4.RouletteWheel;

class TestRoulette {

	@Rule
    public final Stopwatch stopwatch = new Stopwatch() {};

	@Test
	public void testSeenAll36Numbers() {
		RouletteWheel rw = new RouletteWheel();
		Set<Integer> seen = new HashSet<Integer>();
		
		for (int i = 0; i < 1_000_000; i++) {
			int num = rw.spin().getNum();
            seen.add(num);
		} 

		assertTrue("Seen all numbers", seen.size() == (RouletteWheel.MAX-RouletteWheel.MIN+1));
	}
    
    @Test
	public void testSpinMinMaxNumbers() {
		int[] nums = new int[1_000_000];
		RouletteWheel rw = new RouletteWheel();
		for (int i = 0; i < nums.length; i++)
			nums[i] = rw.spin().getNum();
		
		Arrays.sort(nums);
		assertTrue("smallest num == MIN", nums[0] == RouletteWheel.MIN);
		assertTrue("largest num == MAX", nums[nums.length-1] == RouletteWheel.MAX);
	}
	
	@Test 
	public void testOneSpinTimeTaken() {
		RouletteWheel rw = new RouletteWheel();
		
        long before = stopwatch.runtime(TimeUnit.MILLISECONDS);
        rw.spin();
        long after  = stopwatch.runtime(TimeUnit.MILLISECONDS);
        
        assertTrue("Spin takes less than 10 ms", (after - before) <= 10);
	}
	
	@Test (timeout = 10)
	public void testOneSpinTimeTakenVersion2() {
		RouletteWheel rw = new RouletteWheel();
        rw.spin();
	}
	
	@Test
	public void testNumberDistributionWithinOneThousand() {
		int[] numsDistribution = new int[RouletteWheel.MAX+1];
		RouletteWheel rw = new RouletteWheel();
		
		for (int i = 0; i < 1_000_000; i++)
			numsDistribution[rw.spin().getNum()]++;

		Arrays.sort(numsDistribution);
		assertTrue("Most commen and least common vary by less than 0.1%",
			(numsDistribution[0]+1000) > numsDistribution[numsDistribution.length-1]);
	}
}
