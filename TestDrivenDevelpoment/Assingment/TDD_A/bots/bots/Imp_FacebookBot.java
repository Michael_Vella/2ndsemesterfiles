package bots;

import java.util.ArrayList;
import java.util.List;

import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.Post;
import facebook4j.Reading;
import facebook4j.ResponseList;
import facebook4j.auth.AccessToken;

public class Imp_FacebookBot implements I_FacebookBot {
	
	@Override
	public String getFbPosts(I_FacebookBot fb, int max_posts) throws FacebookException {
		Facebook facebook = new FacebookFactory().getInstance();
 	    
		facebook.setOAuthAppId("480610272356824", "6f5d125cb2c77d8364e9b5359f60fb93");
		// Retrieved from
		// https://developers.facebook.com/tools/explorer
		
		
		String accessTokenString = "EAACEdEose0cBANOLnylwb2JK7mZCado8zteuvcjlI5HaPJZCB3MF4ZCBuZATGsknGwHs5uWLLUD9H4wYB2zDmSfpEZCZCbzzLZBxt1ZB7ZBtZAV452C8e8IhxN2XIQ4CijvwfDyFiP2ZAu7aZA1euunjf8TcK3atQMAWDNAqxJ9V5nTd36StHass2P8x1GaGEsaG0mr1bSZAF6IxDjAZDZD";
		AccessToken at = new AccessToken(accessTokenString);
		// Set access token.
			facebook.setOAuthAccessToken(at);
			 
			ResponseList<Post> feeds;
			String posts[] = new String[max_posts]; 
			String allPosts = "";
			try {
				feeds = facebook.getFeed("187446750783", new Reading().limit(max_posts));//get 5 posts from IBM Watson fb page
		        for (int i = 0; i < feeds.size(); i++) {
		            // Get post
		            Post post = feeds.get(i);
		            
		            String message = post.getMessage();
		            
		            //put message post into the posts array to be concatenated later on.
		            posts[i] = message;
		            allPosts = "IBM Watson: " + String.join("\nIBM Watson: ", posts);

		        }              
			} catch (FacebookException e) {
				 allPosts = e.getErrorMessage();
			}
			return allPosts;
	}


	
}
