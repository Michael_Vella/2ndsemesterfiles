﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealEstateGitHub.Models
{
    public class PropertyModel
    {
        [Required]
        public String Reference { get; set; }


        public String Locality { get; set; } = "Malta";

        [Display(Name = "Property Type")]
        public String PropertyType { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "The number of bedrooms must be 0 or more!")]
        [Display(Name = "Bedrooms")]
        public int Bedrooms { get; set; } = 0;

        [Display(Name = "Contract Type")]
        public String ContractType { get; set; }

        public String Description { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "The floor area must be 0 or more!")]
        public int FloorArea { get; set; }

        [Range(0.0, (double)decimal.MaxValue, ErrorMessage = "The price must be 0 or more!")]
        public decimal Price { get; set; }

        public String ImageURL { get; set; }


        // Build a constructor
        public PropertyModel(String reference = null, String locality = "Malta", String propertyType = null, int bedrooms = 0, String contractType = null, String description = null, int floorArea = 0, decimal price = 0.0M)
        {
            this.Reference = reference;
            this.Locality = locality;
            this.PropertyType = propertyType;
            this.Bedrooms = bedrooms;
            this.ContractType = contractType;
            this.Description = description;
            this.FloorArea = floorArea;
            this.Price = price;
        }
    }
}