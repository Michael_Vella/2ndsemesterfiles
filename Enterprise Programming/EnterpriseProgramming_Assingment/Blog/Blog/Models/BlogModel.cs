﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class BlogModel
    {
        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BlogID { get; set; }
       // [Required]
        public string BlogHeading { get; set; }
       // [Required]
        public string BlogPost { get; set; }
       // [Required]
        public string BlogAuthor { get; set; }
        //[Required]
        public string BlogImage { get; set; }
        //public List<string> Tags { get; set; }
        
        public virtual CatagoryModel Catagory { get; set; }

        //Constructors
        public BlogModel() { }


    }
}