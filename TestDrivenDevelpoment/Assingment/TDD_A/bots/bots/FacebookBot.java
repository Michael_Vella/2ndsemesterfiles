package bots;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;

import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.Post;
import facebook4j.ResponseList;
import facebook4j.auth.AccessToken;

public class FacebookBot extends Bot {

	List<String> params = new ArrayList<String>();
	I_FacebookBot fb = null;
	public FacebookBot(List<String> params) {
		super(params);
		this.params = params;
	}
	
	/**
	 * Returns posts
	 */
	public boolean execute(I_FacebookBot fb) {
		
		if(params.size() == 2 && !params.contains(null)) {
			//Regex being used to make sure that only numbers are used
		    String pattern = "(\\d)";
		    Pattern r = Pattern.compile(pattern);
		    Matcher m = r.matcher(params.get(1));
		    if(m.find()) {
		    	final int max_posts = Integer.parseInt(params.get(1));
			    if(!(max_posts <= 0)) {
					try {
						SetOutCome(fb.getFbPosts(fb, max_posts));
						return true;
					}
					catch(FacebookException FBe){
						SetOutCome(FBe.getMessage());
						return false;
					}
				}else {
					SetOutCome("Post amount was less than or equal to 0");
					return false;
				}
		    }
		    else {
		    	SetOutCome("Letters were entered");
		    	return false;
		    }
		}else {
			SetOutCome("There should only be 2 params or null value entered");
			return false;
		}

	}

	@Override
	public boolean execute() {
		return execute(new Imp_FacebookBot());//getting implementation of fbPosts
	}
	
	@Override
	public String getOutCome() {
		return setOutCome;
	}

	
}
