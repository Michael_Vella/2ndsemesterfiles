package bots;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;

import org.junit.Test;

public class Test_MailBot {
	
	List<String> params = new ArrayList<String>();
	MailBot mailBot;
	
	/**
	 * Send a normal Email
	 */
	@Test
	public void SendEmail() {
		params = Arrays.asList("MailBot", "Email", "Content");
		mailBot = new MailBot(params);
		assertTrue(mailBot.execute());
		System.out.println(mailBot.getOutCome());
	}
	
	/***
	 * Using the bot factory
	 */
	@Test
	public void  BotFactory(){
		params = Arrays.asList("MailBot", "Email", "Content");
		Bot bot = BotFactory.createBot(params); 
		boolean isExecuted = bot.execute();
		assertTrue(isExecuted);
		System.out.println(bot.getOutCome());
	}
	
	
	/**
	 * Meant to fail
	 */
	@Test 
	public void SendEmail_ParamsCheck() {
		params = Arrays.asList("MailBot", null);
		mailBot = new MailBot(params);
		assertFalse(mailBot.execute());
		System.out.println(mailBot.getOutCome());
	}
	
	/**
	 * Ment to throw exception
	 */
	@Test
	public void SendEmail_Exception() throws MessagingException{
		params = Arrays.asList();
		mailBot = new MailBot(params);
		assertFalse(mailBot.execute(new Imp_MailBot(), params));
		System.out.println(mailBot.getOutCome());
	}
	
	//Stubs Testing 
	@Test
	public void Send_EmailStub_Pass() {
		params = Arrays.asList("MailBot", "Email", "Content");
		mailBot = new MailBot(params);
		Stb_MailBot_Pass stub = new Stb_MailBot_Pass();
		assertTrue(mailBot.execute(stub,params));
		System.out.println(mailBot.getOutCome());
	}

	@Test
	public void Send_EmailStub_Fail() {
		params = Arrays.asList("MailBot", "Email", "Content");
		mailBot = new MailBot(params);
		Stb_MailBot_Fail stub = new Stb_MailBot_Fail();
		assertFalse(mailBot.execute(stub,params));
		System.out.println(mailBot.getOutCome());
	}
	
	@Test
	public void Send_EmailStub_Exception() throws MessagingException{
		params = Arrays.asList("MailBot", "Email", "Content");
		mailBot = new MailBot(params);
		Stb_MailBot_MessageException stub = new Stb_MailBot_MessageException();
		assertFalse(mailBot.execute(stub,params));
		System.out.println(mailBot.getOutCome());
	}

}
