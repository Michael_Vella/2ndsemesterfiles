﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Lab_Gen4.Models;

namespace Lab_Gen4.DataAccessLayer
{
    public class ProductInitializer : DropCreateDatabaseIfModelChanges<ProductsContext>
    {
        protected override void Seed(ProductsContext context)
        {
            base.Seed(context);
            List<Category> categories = new List<Category>
            {
                new Category{ ID = 1, Name = "Food"},
                new Category{ ID = 2, Name = "Lasgna"},
                new Category{ ID = 3, Name = "Trixie Mattele"},
                new Category{ ID = 4, Name = "Katya"}
            };
        }
    }
}