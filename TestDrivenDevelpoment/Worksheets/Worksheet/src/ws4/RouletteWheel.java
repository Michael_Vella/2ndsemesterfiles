package ws4;

import java.util.Random;

public class RouletteWheel {
	public static final int MIN = 0;
	public static final int MAX = 36;
	Random rand = new Random();
	
	public BallPosition spin() {
		int numb = rand.nextInt(MAX+1);//starts at zero that is why there is a plus 1 
		return new BallPosition(numb);
	}

}
