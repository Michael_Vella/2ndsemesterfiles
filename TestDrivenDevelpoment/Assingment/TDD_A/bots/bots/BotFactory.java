package bots;
import java.util.List;

public class BotFactory {
	/**
	 * Instantiates the bot based on the first index of params.
	 * Bot name must be: FacebookBot, MailBot or WeatherBot in order to work.
	 * @param params
	 * @return initialised bot type based on the bot name
	 */
	public static Bot createBot (List<String>params) {
		String val = params.get(0);
		if(val != null || val == "" ) {
		switch(params.get(0)){
			case "FacebookBot":
				  FacebookBot fb = new FacebookBot(params);
				  return fb;
			case "MailBot":
				  MailBot mb = new MailBot(params);
				  return mb;
			case "WeatherBot":
				  WeatherBot wb = new WeatherBot(params);
				  return wb;
			default:
				System.out.print("No suitable bot name provided");
				return null;
			}
		}
		else {
			System.out.println("Cannot have an empty bot name");
			return null;
		}
		
	}
}
