use vw;
go

create schema cubeSalesStar;
go

create table cubeSalesStar.[date] (
	dateKey uniqueidentifier primary key default newid(),
	yearValue numeric(4,0) not null,
	quarterValue numeric(1,0) not null,
	monthValue numeric(2,0) not null,
	dayoftheMonth numeric(2,0) not null,
	dayoftheWeek numeric(1,0) not null,
	dateValue date unique not null
);

create table cubeSalesStar.store (
	storeKey uniqueidentifier primary key default newid(),
	country nvarchar(256) not null,
	region nvarchar(256) not null,
	city nvarchar(256) not null,
	storeNumber nvarchar(256) unique not null,
	storeDesc nvarchar(max) not null,
	storeId uniqueidentifier not null
);

create table cubeSalesStar.vehicle (
	vehicleKey uniqueidentifier primary key default newid(),
	manufacturer nvarchar(256) not null,
	model nvarchar(256) not null,
	releaseYear numeric(4,0) not null,
	package nvarchar(256) not null,
	engineid nvarchar(256) unique not null,
	chassisid nvarchar(256) unique not null,
	vehicleDesc nvarchar(max) not null,
	vehicleId uniqueidentifier unique not null
)

create table cubeSalesStar.client (
	clientKey uniqueidentifier primary key default newid(),
	country nvarchar(256) not null,
	region  nvarchar(256) not null,
	city  nvarchar(256) not null,
	gender char(1) not null,
	age numeric(3,0) not null,
	passportNumber nvarchar(25) not null,
	fromDate date not null,
	toDate date,
	clientDesc nvarchar(max) not null,
	clientId uniqueidentifier not null,
	unique(clientId, fromDate),
	unique(passportNumber, fromDate)
)

create table cubeSalesStar.sale (
	saleKey uniqueidentifier primary key default newid(),
	productionCost numeric(9,2) not null,
	transportationCost numeric(9,2) not null,
	retailPrice numeric(9,2) not null,
	basePrice numeric(9,2) not null,
	taxAmount numeric(9,2) not null,
	discountAmount numeric(9,2) not null,
	dateKey uniqueidentifier not null references cubeSalesStar.[date](dateKey),
	vehicleKey uniqueidentifier not null references cubeSalesStar.vehicle(vehicleKey),
	storeKey uniqueidentifier not null references cubeSalesStar.store(storeKey),
	clientKey uniqueidentifier not null references cubeSalesStar.client(clientKey),
	invoiceNumber integer not null unique,
	saleId uniqueidentifier not null unique
);

/* clean-up 

	drop table cubeSalesStar.sale;
	drop table cubeSalesStar.client;
	drop table cubeSalesStar.[date];
	drop table cubeSalesStar.store;
	drop table cubeSalesStar.vehicle;

	drop schema cubeSalesStar;
*/
go


-- ETL 

-- procedure to check whether to update and insert a new record for a client
create procedure cubeSalesStar.sp_registerClient 
	(@country nvarchar(256), @region nvarchar(256), @city nvarchar(256),
	 @gender char(1), @age numeric(3,0), @passportNumber nvarchar(25),
	 @saleDate date, @clientId uniqueidentifier)
as
begin
	
	if (not exists 
		(select clientKey from cubeSalesStar.client
		where country=@country and region=@region and city=@city 
		and gender=@gender and age=@age and passportNumber = @passportNumber))
	begin
		update cubeSalesStar.client
		set toDate = dateadd(day, -1, @saleDate)
		where passportNumber = @passportNumber and toDate is null;

		insert cubeSalesStar.client
		(country, region, city, gender, age, passportNumber, fromDate, toDate, clientDesc, clientId)
		values (@country, @region, @city, @gender, @age, @passportNumber, @saleDate, null, 
				concat(@country,',',@region,',',@city,',',@gender,',',@age,',',@passportNumber,',',@saleDate),
				@clientId);
	end

end
go


begin
	
	set nocount on;

	-- populate date dimension
	insert into cubeSalesStar.[date]
		(yearValue, quarterValue, monthValue, dayoftheMonth, dayoftheWeek,  dateValue)
	(select distinct datepart(year,saledate), datepart(quarter, saledate), 
		datepart(month, saledate), datepart(day, saledate),
		datepart(weekday, saledate), cast(saledate as date)
	from oltp.sale);

	-- populate store dimension
	insert into cubeSalesStar.store
		(country, region, city, storeNumber, storeDesc, storeId)
	(select distinct co.countryName, re.regionName, ci.cityName, st.storeNumber, 
		concat(co.countryName,',',re.regionName,',',ci.cityName,',',st.storeNumber),
		st.storeId
	from oltp.store st inner join oltp.city ci
		on st.cityId = ci.cityId
		inner join oltp.region re
		on ci.regionId = re.regionId
		inner join oltp.country co
		on re.countryId = co.countryId
		inner join oltp.vehicle ve
		on ve.storeId = st.storeId
		inner join oltp.sale sa
		on ve.vehicleId = sa.vehicleId);

	-- populate vehicle dimension
	insert into cubeSalesStar.vehicle
		(manufacturer, model, releaseYear, package, chassisid, engineid, vehicleDesc, vehicleId)
	(select distinct ma.manufacturerName, mo.modelName, mo.releaseYear, pa.packageName,
		ve.chasisId, ve.engineId, 
		concat(ma.manufacturerName,',',mo.modelName,',',mo.releaseYear,',',pa.packageName,
		',',ve.chasisId,',',ve.engineId),
		ve.vehicleId
	from oltp.manufacturer ma inner join oltp.model mo
		on ma.manufacturerId = mo.manufacturerId
		inner join oltp.package pa
		on mo.modelId = pa.modelId
		inner join oltp.vehicle ve
		on ve.packageId = pa.packageId
		inner join oltp.sale sa
		on ve.vehicleId = sa.vehicleId);

	-- populate client dimension
	declare cl_cursor cursor for
		select co.countryName, re.regionName, ci.cityName,
			datediff(year, cl.dateOfBirth, sa.saleDate),
			cl.gender, cl.passportNumber, cast(sa.saleDate as date),
			cl.clientId
		from oltp.client cl inner join oltp.city ci
			on cl.cityId = ci.cityId
			inner join oltp.region re
			on ci.regionId = re.regionId
			inner join oltp.country co
			on re.countryId = co.countryId
			inner join oltp.sale sa
			on cl.clientId = sa.clientId
		order by sa.saleDate;

	declare @country nvarchar(256);
	declare @region nvarchar(256);
	declare @city nvarchar(256);
	declare @age numeric(3,0);
	declare @gender char(1);
	declare @passportNumber nvarchar(25);
	declare @saleDate date;
	declare @clientId uniqueidentifier;

	open cl_cursor
		fetch next from cl_cursor into
		@country, @region, @city, @age, @gender, @passportNumber, @saleDate, @clientId

	while @@FETCH_STATUS = 0
	begin
		exec cubeSalesStar.sp_registerClient
			@country, @region, @city, @gender, @age, @passportNumber, @saleDate, @clientId;

		fetch next from cl_cursor into
		@country, @region, @city, @age, @gender, @passportNumber, @saleDate, @clientId
	end

	close cl_cursor;
	deallocate cl_cursor;

	-- populate fact table
		insert into cubeSalesStar.sale 
		(productionCost, transportationCost, retailPrice, basePrice, taxAmount,
		discountAmount, invoiceNumber, saleId, dateKey, storeKey, vehicleKey, clientKey)
	select pa.productionCost, ve.travelCost, ve.retailPrice, sa.basePrice,
		sa.taxAmount, sa.discountAmount, sa.invoiceNumber, sa.saleId,
		(select dateKey from cubeSalesStar.[date] where dateValue=cast(sa.saleDate as date)),
		(select storeKey from cubeSalesStar.store where storeId = ve.storeId),
		(select vehicleKey from cubeSalesStar.vehicle where vehicleId = ve.vehicleId),
		(select clientKey from cubeSalesStar.client where clientId=sa.clientId and
			((toDate is null and sa.saleDate>=fromDate) or
			 (toDate is not null and sa.saleDate between fromDate and toDate)))
	from oltp.sale sa inner join oltp.vehicle ve
		on sa.vehicleId = ve.vehicleId
		inner join oltp.package pa
		on ve.packageId = pa.packageId;

end



-- show profit per year

select d.yearValue, sum(retailPrice - productionCost - transportationCost - taxAmount - discountAmount)  as 'total'
from cubeSalesStar.sale s inner join cubeSalesStar.[date] d
	on s.dateKey = d.dateKey
group by d.yearValue;

select * from cubeSalesStar.date;


