select da.year, da.month, ga.sport, ga.league, ac.market, ac.oddsPortal, count(*)
from odds.bet bt inner join odds.game ga
	on bt.gameKey = ga.gameKey
	inner join odds.account ac
	on bt.accountKey = ac.accountKey
	inner join odds.[date] da
	on bt.dateKey = da.dateKey
group by  rollup(da.year, da.month, ga.sport, ga.league, ac.market, ac.oddsPortal);

select da.year, da.month, ga.sport, ga.league, count(*)
from odds.bet bt inner join odds.game ga
	on bt.gameKey = ga.gameKey
	inner join odds.account ac
	on bt.accountKey = ac.accountKey
	inner join odds.[date] da
	on bt.dateKey = da.dateKey
where market = 'Asian'
group by da.year, da.month, ga.sport, ga.league;