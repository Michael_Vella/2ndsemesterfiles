namespace OnlineShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApiKey2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApiKeyModel",
                c => new
                    {
                        ApiKey = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ApiKey);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ApiKeyModel");
        }
    }
}
