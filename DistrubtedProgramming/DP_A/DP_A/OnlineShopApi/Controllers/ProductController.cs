﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Newtonsoft.Json.Linq;
using OnlineShop.Models;

namespace OnlineShopApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProductController : BaseController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private const string apikey = "00000000-0000-0000-0000-000000000001";

        //Get: api/Product
        [HttpGet]
        public ProductModel GetProductList(int id)
        {
            //using (HttpClient client = new HttpClient())
            //{
            //    client.DefaultRequestHeaders.Add("apikey", apikey);
            //    OnlineShop.DataAccess.ProductsRepository products = new OnlineShop.DataAccess.ProductsRepository();
            //    return products.GetProduct(id);
            //}
            OnlineShop.DataAccess.ProductsRepository products = new OnlineShop.DataAccess.ProductsRepository();
            return products.GetProduct(id);

        }

        // GET: api/Product
        public IQueryable<ProductModel> GetProductModels()
        {
            //HttpResponseMessage responseMsg = Request.CreateResponse();
            //responseMsg = base.ValidateApiKey();
            //if (responseMsg.StatusCode != HttpStatusCode.OK)
            //{
            //    return null;//should cause break
            //}
            //else
            //{
            //    return db.ProductModels;
            //}
            return db.ProductModels;
        }

        // GET: api/Product/5
        [ResponseType(typeof(ProductModel))]
        public IHttpActionResult GetProductModel(int id)
        {
            ProductModel productModel = db.ProductModels.Find(id);
            if (productModel == null)
            {
                return NotFound();
            }

            return Ok(productModel);
        }

        // PUT: api/Product/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProductModel(int id, ProductModel productModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != productModel.ProductModelId)
            {
                return BadRequest();
            }

            db.Entry(productModel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Product
        [ResponseType(typeof(ProductModel))]
        public IHttpActionResult PostProductModel(ProductModel productModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ProductModels.Add(productModel);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = productModel.ProductModelId }, productModel);
        }

        // DELETE: api/Product/5
        [ResponseType(typeof(ProductModel))]
        public IHttpActionResult DeleteProductModel(int id)
        {
            ProductModel productModel = db.ProductModels.Find(id);
            if (productModel == null)
            {
                return NotFound();
            }

            db.ProductModels.Remove(productModel);
            db.SaveChanges();

            return Ok(productModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductModelExists(int id)
        {
            return db.ProductModels.Count(e => e.ProductModelId == id) > 0;
        }

      
    }
}