package bots;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import facebook4j.FacebookException;

public class Test_FacebookBot {

	List<String> params = new ArrayList<String>();
	FacebookBot fb;
	
	@Test
	public void NormalExecute(){
		params = Arrays.asList("FacebookBot", "2");
		fb = new FacebookBot(params);
		assertTrue(fb.execute());
		System.out.println(fb.getOutCome());
	}
	
	@Test
	public void BotFactory(){
		params = Arrays.asList("FacebookBot", "2");
		Bot bot = BotFactory.createBot(params); 
		boolean isExecuted = bot.execute();
		assertTrue(isExecuted);
		System.out.println(bot.getOutCome());
	}
	
	@Test
	public void FB_Posts_NegativeAmt(){
		params = Arrays.asList("FacebookBot", "-1");
		fb = new FacebookBot(params);
		assertFalse(fb.execute());
		System.out.println(fb.getOutCome());
	}
	
	@Test
	public void FB_Posts_Zero(){
		params = Arrays.asList("FacebookBot", "0");
		fb = new FacebookBot(params);
		assertFalse(fb.execute());
		System.out.println(fb.getOutCome());
	}
	/**
	 * Meant to fail
	 */
	@Test 
	public void FacebookBot_ParamsCheck() {
		params = Arrays.asList("FacebookBot", null);
		fb = new FacebookBot(params);
		assertFalse(fb.execute());
		System.out.println(fb.getOutCome());
	}
	
	//Stubs Testing 
	@Test
	public void Facebook_Stub_Pass() throws FacebookException{
		params = Arrays.asList("FacebookBot", "1");
		fb = new FacebookBot(params);
		Stb_FacebookBot_Pass stub = new Stb_FacebookBot_Pass();
		assertTrue(fb.execute(stub));
		System.out.println(fb.getOutCome());
	}

	@Test
	public void FacebookBot_Fail() throws FacebookException {
		params = Arrays.asList("FacebookBot", "1");
		fb = new FacebookBot(params);
		Stb_FacebookBot_Fail stub = new Stb_FacebookBot_Fail();
		assertTrue(fb.execute(stub));
		System.out.println(fb.getOutCome());
	}
		
	@Test
	public void FacebookBot_Stub_FB_Exception() throws FacebookException{
		params = Arrays.asList("FacebookBot", "1");
		fb = new FacebookBot(params);
		Stb_FacebookBot_FacebookException stub = new Stb_FacebookBot_FacebookException();
		assertFalse(fb.execute(stub));
		System.out.println(fb.getOutCome());
	}
		
}