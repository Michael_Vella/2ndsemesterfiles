﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnlineShop.Models;
namespace OnlineShop.DataAccess
{
    public class ProductsRepository : Connection
    {
        public ProductsRepository() : base()
        {
        }

        public ProductModel GetProduct(int id)
        {
             return Entities.ProductModels.SingleOrDefault(x => x.ProductModelId == id);
        }

        public List<ProductModel> GetProductList(int id) {
            return Entities.ProductModels.Where(x => x.ProductModelId == id).ToList();
        }
    }
}