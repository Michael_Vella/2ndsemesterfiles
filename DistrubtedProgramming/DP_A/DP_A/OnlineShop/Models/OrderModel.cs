﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OnlineShop.Models
{
    public class OrderModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderModelId { get; set; }
        public int ProductModelId { get; set; }
        public string UserEmail { get; set; }
        public int OrderQuanity { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderStage { get; set; }

        //Relationships
        public virtual ICollection<ProductModel> products { get; set; }
        //public virtual ICollection<ApplicationUser> applicationUsers { get; set; }

    }
}