use vw;
go

drop table cubeProductionStar.production;
drop table cubeProductionStar.[date];
drop table cubeProductionStar.store;
drop table cubeProductionStar.vehicle;

create schema cubeProductionStar;
go

create table cubeProductionStar.[date] (
	dateKey uniqueidentifier primary key default newsequentialid(),
	yearValue numeric(4,0) not null,
	quarterValue numeric(1,0) not null,
	monthValue numeric(2,0) not null,
	dayValue numeric(2,0) not null,
	weekDayValue numeric(1,0) not null,
	dateValue date unique not null
);

create table cubeProductionStar.store (
	storeKey uniqueidentifier primary key default newsequentialid(),
	country nvarchar(256) not null,
	region nvarchar(256) not null,
	city nvarchar(256) not null,
	storeNumber numeric(2,0) unique not null,
	storeDesc nvarchar(max) not null,
	storeId uniqueidentifier unique not null
);

create table cubeProductionStar.vehicle (
	vehicleKey uniqueidentifier primary key default newsequentialid(),
	manufacturer nvarchar(256) not null,
	model nvarchar(256) not null,
	releaseYear numeric(4,0) not null,
	package nvarchar(256) not null,
	chassisId nvarchar(256) not null unique,
	engineId nvarchar(256) not null unique,
	vehichleDesc nvarchar(max) not null,
	vehicleId uniqueidentifier unique not null
);

create table cubeProductionStar.production (
	productionKey uniqueidentifier primary key default newsequentialid(),
	productionCost numeric(9,2) not null,
	retailPrice numeric(9,2) not null,
	transportCost numeric(9,2) not null,
	dateKey uniqueidentifier references cubeProductionStar.[date](dateKey) not null,
	vehicleKey uniqueidentifier references cubeProductionStar.vehicle(vehicleKey) not null,
	storeKey uniqueidentifier references cubeProductionStar.store(storeKey) not null
);


insert into cubeProductionStar.[date]
	(yearValue, quarterValue, monthValue, dayValue, weekDayValue, dateValue)
select distinct datepart(year,manufacturedDate), datepart(quarter,manufacturedDate),
	datepart(month,manufacturedDate), datepart(day,manufacturedDate),
	datepart(weekday,manufacturedDate), manufacturedDate
from oltp.vehicle;

insert into cubeProductionStar.store
	(country, region, city, storeNumber, storeDesc, storeId)
select distinct countryName, regionName, cityName, storeNumber, 
	concat( countryName,',',regionName,',',cityName,',',storeNumber),
	st.storeId
from oltp.country cn inner join oltp.region re
	on cn.countryId = re.countryId
	inner join oltp.city ci on
	re.regionId = ci.regionId
	inner join oltp.store st
	on ci.cityId = st.cityId
	inner join oltp.vehicle ve
	on st.storeId = ve.storeId;

insert into cubeProductionStar.vehicle
	(manufacturer, model, releaseYear, package, chassisId, engineId, vehichleDesc, vehicleId)
select distinct manufacturerName, modelName, releaseYear, packageName, chasisId, engineId,
	concat(manufacturerName,',', modelName,',', releaseYear,',', packageName,',', chasisId,',', engineId),
	ve.vehicleId
from oltp.manufacturer ma inner join oltp.model mo
	on ma.manufacturerId = mo.manufacturerId
	inner join oltp.package pk 
	on mo.modelId = pk.modelId
	inner join oltp.vehicle ve
	on pk.packageId = ve.packageId;

insert into cubeProductionStar.production
	(productionCost, retailPrice, transportCost, dateKey, vehicleKey, storeKey)
select productionCost, retailPrice, travelCost,
	(select datekey from cubeProductionStar.[date] where datevalue = manufacturedDate),
	(select vehicleKey from cubeProductionStar.vehicle where vehicleId = ve.vehicleId),
	(select storeKey from cubeProductionStar.store where storeId = ve.storeId)
from oltp.vehicle ve inner join oltp.package pa
	on ve.packageId = pa.packageId;

select da.yearValue, ve.manufacturer, ve.model, count(*) as 'total'
from cubeProductionStar.production pr inner join cubeProductionStar.[date] da
	on pr.dateKey = da.dateKey
	inner join cubeProductionStar.vehicle ve
	on pr.vehicleKey = ve.vehicleKey
group by da.yearValue, ve.manufacturer, ve.model;