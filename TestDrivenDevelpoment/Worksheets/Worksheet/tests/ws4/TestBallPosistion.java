package ws4;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.Assert.assertEquals;
import static ws4.BallPosition.Colour.BLACK;
import static ws4.BallPosition.Colour.GREEN;
import static ws4.BallPosition.Colour.RED;
import static ws4.RouletteWheel.MAX;
import static ws4.RouletteWheel.MIN;

import org.junit.Test;

class TestBallPosistion {


	@Test
	public void testGetColour() {
		BallPosition[] pos = new BallPosition[MAX+1];
		for (int i = MIN; i <= MAX; i++)
			pos[i] = new BallPosition(i);
		
		assertEquals(GREEN, pos[0].getColour());

		assertEquals(RED,	pos[1].getColour());
		assertEquals(BLACK, pos[2].getColour());
		assertEquals(RED,	pos[3].getColour());
		assertEquals(BLACK, pos[4].getColour());
		assertEquals(RED,	pos[5].getColour());
		assertEquals(BLACK, pos[6].getColour());
		assertEquals(RED,	pos[7].getColour());
		assertEquals(BLACK, pos[8].getColour());
		assertEquals(RED,	pos[9].getColour());
		assertEquals(BLACK, pos[10].getColour());

		assertEquals(BLACK, pos[11].getColour());
		assertEquals(RED,	pos[12].getColour());
		assertEquals(BLACK, pos[13].getColour());
		assertEquals(RED,	pos[14].getColour());
		assertEquals(BLACK, pos[15].getColour());
		assertEquals(RED,	pos[16].getColour());
		assertEquals(BLACK, pos[17].getColour());
		assertEquals(RED,	pos[18].getColour());

		assertEquals(RED,	pos[19].getColour());
		assertEquals(BLACK, pos[20].getColour());
		assertEquals(RED,	pos[21].getColour());
		assertEquals(BLACK, pos[22].getColour());
		assertEquals(RED,	pos[23].getColour());
		assertEquals(BLACK, pos[24].getColour());
		assertEquals(RED,	pos[25].getColour());
		assertEquals(BLACK, pos[26].getColour());
		assertEquals(RED,	pos[27].getColour());
		assertEquals(BLACK, pos[28].getColour());

		assertEquals(BLACK, pos[29].getColour());
		assertEquals(RED,	pos[30].getColour());
		assertEquals(BLACK, pos[31].getColour());
		assertEquals(RED,	pos[32].getColour());
		assertEquals(BLACK, pos[33].getColour());
		assertEquals(RED,	pos[34].getColour());
		assertEquals(BLACK, pos[35].getColour());
		assertEquals(RED,	pos[36].getColour());
	}
}
