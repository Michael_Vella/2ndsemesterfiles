﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineShop.Models;
namespace DataAccess
{
    public class ProductsRepo : ConnectionClass
    {
        public ProductsRepo() : base() { }

        public IQueryable<ProductModel> GetProduct(int id)
        {
            return Entity.ProductModels.Where(x => x.ProductModelId == id);
        }
    }
}
