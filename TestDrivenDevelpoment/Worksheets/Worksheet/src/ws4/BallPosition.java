package ws4;

public class BallPosition {
	public enum Colour {GREEN, RED, BLACK};
	
	private int num;

	public int getNum() {
		return num;
	}
	
	public BallPosition(int number) {
		this.num = number;
	}
	
	public Colour getColour() {
		if (num == 0) {
			return Colour.GREEN;
		}
		else if(num >= 10 || (num <= 19 && num >=28)) {
			if(num % 2 == 0) //if modulus returns a zero == even number
				return Colour.BLACK;
			else
				return Colour.RED;
		}
		else {
			if(num % 2 == 0)
				return Colour.RED;
			else
				return Colour.BLACK;
		}
	}
}
