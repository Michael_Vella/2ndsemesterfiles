package bots;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

public class Test_WeatherBot {
	
	List<String> params = new ArrayList<String>();
	WeatherBot weatherBot;
	
	/**
	 * Retrieve weather
	 */
	@Test
	public void GetWeather() {
		params = Arrays.asList("WeatherBot", "Valletta");
		weatherBot = new WeatherBot(params);
		assertTrue(weatherBot.execute());
		System.out.println(weatherBot.getOutCome());
	}
	
	/***
	 * Using the bot factory
	 */
	@Test
	public void  BotFactory(){
		params = Arrays.asList("WeatherBot", "London");
		Bot bot = BotFactory.createBot(params); 
		boolean isExecuted = bot.execute();
		assertTrue(isExecuted);
		System.out.println(bot.getOutCome());
	}
	
	
	/**
	 * Meant to fail
	 */
	@Test 
	public void WeatherBot_ParamsCheck() {
		params = Arrays.asList("WeatherBot", null);
		weatherBot = new WeatherBot(params);
		assertFalse(weatherBot.execute());
		System.out.println(weatherBot.getOutCome());
	}
	
	/**
	 * Ment to throw exception
	 * @throws IOException 
	 */
	@Test
	public void WeatherBot_Exception() throws IOException{
		params = Arrays.asList();
		weatherBot = new WeatherBot(params);
		assertFalse(weatherBot.execute(new Imp_WeatherBot(), params));
		System.out.println(weatherBot.getOutCome());
	}
	
	/**
	 * Ment to throw exception
	 * @throws IOException 
	 */
	@Test
	public void WeatherBot_FakeCity() throws IOException{
		params = Arrays.asList("WeatherBot","sdignosdnpogisfdignsdigpdsnfgnspdgfnps");
		weatherBot = new WeatherBot(params);
		assertTrue(weatherBot.execute(new Imp_WeatherBot(), params));
		System.out.println(weatherBot.getOutCome());
	}
	
	//Stubs Testing 
	@Test
	public void WeatherBot_Stub_Pass() throws IOException {
		params = Arrays.asList("WeatherBot", "London");
		weatherBot = new WeatherBot(params);
		Stb_WeatherBot_Pass stub = new Stb_WeatherBot_Pass();
		assertTrue(weatherBot.execute(stub,params));
		System.out.println(weatherBot.getOutCome());
	}

	@Test
	public void WeatherBot_Stub_Fail() throws IOException {
		params = Arrays.asList("WeatherBot", "London");
		weatherBot = new WeatherBot(params);
		Stb_WeatherBot_Fail stub = new Stb_WeatherBot_Fail();
		assertTrue(weatherBot.execute(stub,params));
		System.out.println(weatherBot.getOutCome());
	}
	
	@Test
	public void WeatherBot_Stub_IOException() throws IOException{
		params = Arrays.asList("WeatherBot", "London");
		weatherBot = new WeatherBot(params);
		Stb_WeatherBot_IOEx stub = new Stb_WeatherBot_IOEx();
		assertFalse(weatherBot.execute(stub,params));
		System.out.println(weatherBot.getOutCome());
	}
	
	@Test
	public void WeatherBot_Stub_JsonEx() throws JSONException{
		params = Arrays.asList("WeatherBot", "London");
		weatherBot = new WeatherBot(params);
		Stb_WeatherBot_JsonEx stub = new Stb_WeatherBot_JsonEx();
		assertFalse(weatherBot.execute(stub,params));
		System.out.println(weatherBot.getOutCome());
	}

}
