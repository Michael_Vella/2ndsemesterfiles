package bots;

import java.util.List;
import facebook4j.FacebookException;

public interface I_FacebookBot {
	
	/**
	 * Returns Facebook posts based on the passed Fb ID and max post.
	 */
	public String getFbPosts(I_FacebookBot fb, int max_posts) throws FacebookException;

}
