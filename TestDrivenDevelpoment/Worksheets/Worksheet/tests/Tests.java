import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Tests {

	@Test
	void testAddPlayer() {
		Player p = new Player(0, "Vella", PlayerRole.MDF);
		Team t = new Team();
		t.addPlayer(p);
		assertEquals("Player is added", 1, t.teamCount());
	}
	
	@Test
	void testRemovePlayer() {
		Player p = new Player(0, "Vella", PlayerRole.MDF);
		Team t = new Team();
		t.addPlayer(p);
		t.removePlayer(p.getNumber());
		assertEquals("Player is removed", 0, t.teamCount());
	}
	

	@Test
	void testRemoveNonPlayer() {
		Player p = new Player(0, "Vella", PlayerRole.GKP);
		Team t = new Team();
		t.addPlayer(p);
		t.removePlayer(12);
		assertEquals("Fake remove does not affect", 1, t.teamCount());
	}
	

	@Test
	void testAddSamePlayer() {
		Player p = new Player(0, "Vella", PlayerRole.MDF);
		Player p2 = new Player(0, "Vella", PlayerRole.MDF);
		Team t = new Team();
		t.addPlayer(p);
		t.addPlayer(p2);
		assertEquals("Same Player should not be added ", 1, t.teamCount());
	}
	
	@Test
	void testAdd5Players() {
		Player p = new Player(0, "Vella", PlayerRole.MDF);
		Player p2 = new Player(1, "Vella", PlayerRole.STK);
		Player p3 = new Player(2, "Vella", PlayerRole.STK);
		Player p4 = new Player(3, "Vella", PlayerRole.STK);
		Player p5 = new Player(4, "Vella", PlayerRole.GKP);
		Team t = new Team();
		t.addPlayer(p);
		t.addPlayer(p2);
		t.addPlayer(p3);
		t.addPlayer(p4);
		t.addPlayer(p5);
		assertEquals("Should have 5 ", 5, t.teamCount());
	}


}
