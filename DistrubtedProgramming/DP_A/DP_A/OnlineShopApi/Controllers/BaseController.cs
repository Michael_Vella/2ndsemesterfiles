﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OnlineShop.DataAccess;
namespace OnlineShopApi.Controllers
{
    public class BaseController : ApiController
    {
        public BaseController() : base() { }

        protected HttpResponseMessage ValidateApiKey()
        {
            //Api key used 00000000-0000-0000-0000-000000000001
            HttpResponseMessage responseMsg = Request.CreateResponse(HttpStatusCode.OK);
            if (!Request.Headers.Contains("apikey"))
            {
                responseMsg = Request.CreateResponse(HttpStatusCode.Unauthorized, new { Error = "No api key providedd" });
            }
            else
            {
                string apikey = Request.Headers.GetValues("apikey").First();
                if (!(new ApiKeyRepository().ValidateApiKey(new Guid(apikey))))
                {
                    responseMsg = Request.CreateResponse(HttpStatusCode.Unauthorized, new { Error = "Invalid Api Key" });
                }
            }
            return responseMsg;
        }
    }
}
