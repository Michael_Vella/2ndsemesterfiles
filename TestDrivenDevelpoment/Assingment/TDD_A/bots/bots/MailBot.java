package bots;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

public class MailBot extends Bot {
	private List<String> params = new ArrayList<String>();
	
	public MailBot(List<String> params) {
		super(params);
		this.params = params;
	}
	
	public boolean execute(I_Mail mailBot,List<String> params) {
		if(params.contains(null)) {
			SetOutCome("Mail content can not be null");
			return false;
		}
		else {
			try {
				if(!mailBot.sendEmail(mailBot, params))
					return false;
			}
			catch(MessagingException me) {
				SetOutCome(me.getMessage());
				return false;
			}
			SetOutCome("Mail has been sent");
			return true;
		}
	}
	
	@Override
	public boolean execute() {
		return execute (new Imp_MailBot(),params);
	}

	@Override
	public String getOutCome() {
		
		return setOutCome;
	}

}