﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class BlogDbContext : DbContext
    {
        public BlogDbContext() : base("BlogDbContext") { }

        public DbSet<CatagoryModel> catagories { get; set; }
        public DbSet<BlogModel> blogs { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}