﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnlineShop.Models;
namespace OnlineShop.DataAccess
{
    public class Connection
    {
        public  ApplicationDbContext Entities { get; set; }

        public Connection() {
            Entities = new ApplicationDbContext();
        }
    }
}