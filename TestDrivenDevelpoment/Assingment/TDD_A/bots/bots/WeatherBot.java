package bots;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import net.aksingh.owmjapis.CurrentWeather;
import net.aksingh.owmjapis.OpenWeatherMap;

public class WeatherBot extends Bot {

	private List<String> params = new ArrayList<String>();
	
	public WeatherBot(List<String> args) {
		super(args);
		this.params = args;
	}
	
	public boolean execute(I_WeatherBot weather, List<String> params) { 
		if(params.isEmpty() || params.contains(null)) {
			SetOutCome("There can not be null values or empty strings ");
			return false;
		}
		try {
			String city = params.get(1);	
			SetOutCome(weather.getWeather(weather, city));
			return true;
		}catch (IOException ex) {
			SetOutCome(ex.getMessage());
			return false;
		}
		catch (JSONException ex) {
			SetOutCome(ex.getMessage());
			return false;
		}
	}
	
	@Override
	public boolean execute()  {
		return execute(new Imp_WeatherBot(), params);
	}
	
	/**
	 * Returns the out come 
	 */
	@Override
	public String getOutCome() {
		return setOutCome;
	}

}
