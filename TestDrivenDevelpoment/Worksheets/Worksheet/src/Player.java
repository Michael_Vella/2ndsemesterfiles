
public class Player {
	private int number;
	private String surname;
	private PlayerRole role;
	
	/**
	 * 
	 * @param number
	 * @param surname
	 * @param role
	 */
	Player(int number, String surname, PlayerRole role){
		this.number = number;
		this.surname = surname;
		this.role = role;
	}
	
	//Getters and Setters
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public PlayerRole getRole() {
		return role;
	}
	
	public void setRole(PlayerRole role) {
		this.role = role;
	}
}
