package bots;

import java.util.List;

import javax.mail.MessagingException;

public interface I_Mail {
	
	/**
	 * Executes the functionality of the bot
	 * @return
	 * @throws MessagingException 
	 */
	boolean sendEmail(I_Mail mailBot, List<String> params) throws MessagingException;

}
