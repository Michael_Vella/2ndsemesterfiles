﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineShop.DataAccess
{
    public class ApiKeyRepository : Connection
    {
        public ApiKeyRepository() : base() { }

        //Methods
        public bool ValidateApiKey(Guid apiKey) => Entities.KeyModels.SingleOrDefault(key => key.ApiKey == apiKey) != null ? true : false;

    }
}