/*
	MCAST IICT
	IICT6011 - Business Intelligence & Reporting
	Production creation and ETL
*/
use [vw];
go

-- Clean-up
drop table [cubeProductionStar].[production];
drop table [cubeProductionStar].[date];
drop table [cubeProductionStar].[modelPackage];
drop table [cubeProductionStar].[store];
go

drop schema [cubeProductionStar];
go

-- Creation
create schema [cubeProductionStar];
go

create table [cubeProductionStar].[date] (
	dateKey uniqueidentifier constraint date_dim_key primary key default newsequentialid()
	, yearValue numeric(4,0) not null
	, quarterValue numeric(4,0) not null
	, monthValue numeric(2,0) not null
	, weekDayValue numeric(1,0) not null
	, dayValue numeric(2,0) not null
	, dateValue date constraint date_nat_key unique not null
);

create table [cubeProductionStar].[store] (
	storeKey uniqueidentifier constraint store_dim_key primary key default newsequentialid()
	, country nvarchar(256) not null
	, region nvarchar(256) not null
	, city nvarchar(256) not null
	, storeNumber numeric(2,0) constraint store_nat_key unique not null
	, storeDesc nvarchar(max) not null
	, storeId uniqueidentifier constraint store_oltp_id unique not null
);

create table [cubeProductionStar].[modelPackage] (
	packageKey uniqueidentifier constraint package_dim_key primary key default newsequentialid()
	, manufacturer nvarchar(256) not null
	, model nvarchar(256) not null
	, releaseYear numeric(4,0) not null
	, package nvarchar(256) not null
	, packageDesc nvarchar(max) not null
	, packageId uniqueidentifier constraint package_oltp_id unique not null
	, constraint package_nat_key unique (model, releaseYear, package)
);

create table [cubeProductionStar].[production] (
	productionKey uniqueidentifier constraint production_fact_key primary key default newsequentialid()
	, productionCost numeric(9,2) not null
	, transportationCost numeric(9,2) not null
	, retailPrice numeric(9,2) not null
	, chasisId nvarchar(256) constraint production_nat_key_chasis unique not null
	, engineId nvarchar(256) constraint production_nat_key_engine unique not null
	, dateKey uniqueidentifier not null
		constraint production_date_fk references [cubeProductionStar].[date] (dateKey)
	, packageKey uniqueidentifier not null
		constraint production_modelPackage_fk references [cubeProductionStar].[modelPackage] (packageKey)
	, storeKey uniqueidentifier not null
		constraint production_store_fk references [cubeProductionStar].[store] (storeKey)
	, vehicleId uniqueidentifier constraint production_oltp_id unique not null
);
go

-- ETL
begin
	set nocount on;

	-- Date
	insert into [cubeProductionStar].[date] (yearValue, quarterValue, monthValue, weekDayValue, dayValue, dateValue)
		(select distinct datepart(year,manufacturedDate), datepart(quarter, manufacturedDate)
				,datepart(month, manufacturedDate), datepart(weekday, manufacturedDate)
				, datepart(day, manufacturedDate), manufacturedDate
		from	[oltp].[vehicle]);

	-- Store
	insert into [cubeProductionStar].[store] (country, region, city, storeNumber, storeDesc, storeId)
		(select	distinct cnt.countryName, reg.regionName, cty.cityName, sto.storeNumber
				, concat(cnt.countryName,',', reg.regionName,',', cty.cityName,',', sto.storeNumber) 
				, sto.storeId
		from	[oltp].[country] cnt join [oltp].[region] reg on (reg.countryId=cnt.countryId)
				join [oltp].[city] cty on (cty.regionid=reg.regionId)
				join [oltp].[store] sto on (sto.cityId=cty.cityId)
				join [oltp].[vehicle] veh on (veh.storeId=sto.storeId));

	-- Model Package
	insert into [cubeProductionStar].[modelPackage] (manufacturer, model, releaseYear, package, packageDesc, packageId)
		(select distinct man.manufacturerName, mdl.modelName, mdl.releaseYear, pkg.packageName
				, concat(man.manufacturerName,',', mdl.modelId,',', mdl.releaseYear,',', pkg.packageName)
				, pkg.packageId
		from	[oltp].[manufacturer] man join [oltp].[model] mdl on (mdl.manufacturerId=man.manufacturerId)
				join [oltp].[package] pkg on (pkg.modelId=mdl.modelId)
				join [oltp].[vehicle] veh on (veh.packageId=pkg.packageId));

	-- Production
	insert into [cubeProductionStar].[production] (productionCost, transportationCost, retailPrice, dateKey, storeKey, packageKey
		, engineId, chasisId, vehicleId)
		(select	pkg.productionCost, veh.travelCost, veh.retailPrice
				, (select dateKey from [cubeProductionStar].[date] where dateValue=veh.manufacturedDate)
				, (select storeKey from [cubeProductionStar].[store] where storeId=veh.storeId)
				, (select packageKey from [cubeProductionStar].[modelPackage] where packageId=veh.packageId)
				, veh.engineId, veh.chasisId, veh.vehicleId
		from	[oltp].[vehicle] veh join [oltp].[package] pkg on (pkg.packageId=veh.packageId));
end;

-- Basic reporting
select * from [cubeProductionStar].[date];
select * from [cubeProductionStar].[store];
select * from [cubeProductionStar].[modelPackage];
select * from [cubeProductionStar].[production];
go

-- Cube Report
select	dat.yearValue, dat.quarterValue, pkg.manufacturer, pkg.model, pkg.releaseYear,pkg.package, sto.country, sto.region, sto.city, count(*)
from	[cubeProductionStar].[production] prd join [cubeProductionStar].[date] dat on (prd.dateKey=dat.dateKey)
		join [cubeProductionStar].[store] sto on (prd.storeKey=sto.storeKey)
		join [cubeProductionStar].[modelPackage] pkg on (prd.packageKey=pkg.packageKey)
group by cube (dat.yearValue, dat.quarterValue, pkg.manufacturer, pkg.model, pkg.releaseYear,pkg.package, sto.country, sto.region, sto.city);