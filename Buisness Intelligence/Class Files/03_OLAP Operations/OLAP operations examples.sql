-- OLAP Operations


-- Show the total production by manufacturer for 2016 January
-- slice across 1 dimension
select manufacturer, count(*) as 'total'
from cubeProductionStar.production pr 
	inner join  cubeProductionStar.[date] da
	on pr.dateKey = da.dateKey
	inner join cubeProductionStar.modelPackage mp
	on pr.packageKey = mp.packageKey
where da.monthValue = 1 and da.yearValue = 2016
group by manufacturer;

-- Show the total production by manufacturer, model,
-- releaseYear, package for 2016 January
select manufacturer, model, releaseYear, package, count(*) as 'total'
from cubeProductionStar.production pr 
	inner join  cubeProductionStar.[date] da
	on pr.dateKey = da.dateKey
	inner join cubeProductionStar.modelPackage mp
	on pr.packageKey = mp.packageKey
where da.monthValue = 1 and da.yearValue = 2016
group by manufacturer,model, releaseYear, package;

-- roll up across 1 dimension
select manufacturer, model, releaseYear, package, count(*) as 'total'
from cubeProductionStar.production pr 
	inner join  cubeProductionStar.[date] da
	on pr.dateKey = da.dateKey
	inner join cubeProductionStar.modelPackage mp
	on pr.packageKey = mp.packageKey
where da.monthValue = 1 and da.yearValue = 2016
group by rollup(manufacturer,model, releaseYear, package);


select manufacturer, model, releaseYear, package, count(*) as 'total'
from cubeProductionStar.production pr 
	inner join  cubeProductionStar.[date] da
	on pr.dateKey = da.dateKey
	inner join cubeProductionStar.modelPackage mp
	on pr.packageKey = mp.packageKey
where da.monthValue = 1 and da.yearValue = 2016
group by grouping sets (
	(manufacturer,model, releaseYear, package),
	(manufacturer,model, releaseYear),
	(manufacturer),
	()
);

select manufacturer, model, releaseYear, package, count(*) as 'total',
	grouping_id(manufacturer,model, releaseYear, package)
from cubeProductionStar.production pr 
	inner join  cubeProductionStar.[date] da
	on pr.dateKey = da.dateKey
	inner join cubeProductionStar.modelPackage mp
	on pr.packageKey = mp.packageKey
where da.monthValue = 1 and da.yearValue = 2016
group by grouping sets (
	(manufacturer,model, releaseYear, package),
	(manufacturer,model, releaseYear),
	(manufacturer),
	()
);

-- rollup on 2 dimensions

select manufacturer, model, releaseYear, package,
	st.country, st.region, st.city, count(*) as 'total',
	grouping_id(manufacturer, model, releaseYear, package,st.country, st.region, st.city)
from cubeProductionStar.production pr 
	inner join  cubeProductionStar.[date] da
	on pr.dateKey = da.dateKey
	inner join cubeProductionStar.modelPackage mp
	on pr.packageKey = mp.packageKey
	inner join cubeProductionStar.store st
	on pr.storeKey = st.storeKey
where da.monthValue = 1 and da.yearValue = 2016
group by rollup(manufacturer,model, releaseYear, package),
	rollup(country,region,city);

-- drill down on 2 dimensions

select manufacturer, model, releaseYear, package,
	st.country, st.region, st.city, count(*) as 'total',
	grouping_id(manufacturer, model, releaseYear, package,st.country, st.region, st.city)
from cubeProductionStar.production pr 
	inner join  cubeProductionStar.[date] da
	on pr.dateKey = da.dateKey
	inner join cubeProductionStar.modelPackage mp
	on pr.packageKey = mp.packageKey
	inner join cubeProductionStar.store st
	on pr.storeKey = st.storeKey
where da.monthValue = 1 and da.yearValue = 2016
group by rollup(package, releaseYear, model, manufacturer),
	rollup(city, region, country);

-- rollup across 3 dimensions

select manufacturer, model, releaseYear, package,
	st.country, st.region, st.city, 
	da.yearValue, da.quarterValue, da.monthValue,
	count(*) as 'total',
	grouping_id(manufacturer, model, releaseYear, package,st.country, st.region, st.city)
from cubeProductionStar.production pr 
	inner join  cubeProductionStar.[date] da
	on pr.dateKey = da.dateKey
	inner join cubeProductionStar.modelPackage mp
	on pr.packageKey = mp.packageKey
	inner join cubeProductionStar.store st
	on pr.storeKey = st.storeKey
where da.monthValue = 1 and da.yearValue = 2016
group by rollup(manufacturer,model, releaseYear, package),
		 rollup(country,region,city),
		 rollup(da.yearValue, da.quarterValue, da.monthValue);


-- dice across 2 dimensions
select manufacturer, model, releaseYear, package,
	st.country, st.region, st.city, 
	da.yearValue, da.quarterValue, da.monthValue,
	count(*) as 'total'
from cubeProductionStar.production pr 
	inner join  cubeProductionStar.[date] da
	on pr.dateKey = da.dateKey
	inner join cubeProductionStar.modelPackage mp
	on pr.packageKey = mp.packageKey
	inner join cubeProductionStar.store st
	on pr.storeKey = st.storeKey
where da.quarterValue = 1 and da.yearValue = 2016
	and st.country = 'England'
group by manufacturer,model, releaseYear, package,
		 country,region,city,
		 da.yearValue, da.quarterValue, da.monthValue;


-- pivot

with productionData (manufacturer, manYear, vid)
as
(select mp.manufacturer, da.yearValue, pr.vehicleId
from cubeProductionStar.production pr inner join cubeProductionStar.[date] da
	on pr.dateKey = da.dateKey
	inner join cubeProductionStar.modelPackage mp
	on pr.packageKey = mp.packageKey)
select *
from productionData
	pivot (count(vid) for manYear in ([2015],[2016],[2017])) as p;

