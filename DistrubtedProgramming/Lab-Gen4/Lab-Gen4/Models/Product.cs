﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lab_Gen4.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string Name{ get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public int CategoryID { get; set; }
        public virtual Category category { get; set; }
    }
}