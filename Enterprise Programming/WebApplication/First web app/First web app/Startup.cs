﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(First_web_app.Startup))]
namespace First_web_app
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
