﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lab_Gen4.Models
{
    public class Category
    {
        public int ID { get; set; } // primary key
        public string Name { get; set; }

        public virtual ICollection <Product> Products { get; set; }//Creates a one to many relation ship 
    }
}