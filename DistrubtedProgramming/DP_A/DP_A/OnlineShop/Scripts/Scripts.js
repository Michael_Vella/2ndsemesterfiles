﻿function getProducts() {

    $.getJSON("http://localhost:58320/api/Product/GetProductModels", function (data) {
        var productRow = "";
        $.each(data, function (index, product) {
            productRow += "<tr>" +
                "<td id=\"txtName" + product.productId + "\" >" + product.ProductName + "</td > " +
                "<td id=\>" + product.ProductDescription + "</td>" +
                "<td>" + product.ProductPrice + "</td>" +
                "<td><input type='number' id='txtQty"+ product.ProductModelId + "' placeholder='Quantity' /> <td/>" +
                "<td><input type='button' id='btnAddtoCart" + product.ProductModelId + "' value='Add to cart' onclick='addToCart(" + product.ProductModelId + ")' class='btn'/><td/>"
                "</tr > ";
        });

        $("#products").html(productRow);

    });
}

function addToCart(productId) {

    var myCart = [];
    if (localStorage.getItem("orderedItems") !== null) {
        myCart = JSON.parse(localStorage.getItem("orderedItems"));
    }

    var qty = $("#txtQty" + productId).val();
    
    var flag = false;
    $.each(myCart, function (index, v) {
        if (v.Id === productId) {
            v.Qty += Number(qty);
            flag = true;
            return;
        }
    });

    if (flag === false) {
        localStorage.setItem(parseInt(productId), parseInt(qty));
    }

    var myJsonString = JSON.stringify(myCart);
}


function checkOut() {

     var jsonObject = {
        ProductList: getCart(),
        InvoiceNumber: "INV" + Math.floor((Math.random() * 999999) + 1000000),
        Currency: "EUR",
        Tax: 0,
        ShippingFee: 0,
        SiteURL: window.location.href.split('?')[0]
    }

   
    $.ajax({
        type: "POST",
        url: "http://localhost:58320/api/Paypal/GetPaymentPayPalURL",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(jsonObject),
        dataType: "json",
        error: function (xhr, status, error) {
            console.log(xhr.responseText);
        },
        success: function (responseData) {
            //hideMessage();
            if (responseData.indexOf("ERROR") >= 0) {
              //Add alert
            } else {
                window.location.href = responseData;
            }
        }
    });
}

function payOrders() {
    if (confirm("Confirm payment?")) {
        var params = getCheckoutParams();
        var jsonObject = {
            PayerID: params["payerid"],
            PaymentID: params["paymentid"]
        }

        $.ajax({
            type: "POST",
            url: "http://localhost:58320/api/Paypal/DoPayment",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(jsonObject),
            dataType: "json",
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            },
            success: function (r) {
                alert("Payment successfull ♥");
                localStorage.clear();
                $(location).attr('href', 'http://localhost:58312/Product');
            }
        });
    }
}

function showCart() {
    var jsonObject = getCart();
    for (var i = 0; i < localStorage.length; i++) {
        $.ajax({
            type: "GET",
            url: "http://localhost:58320/api/Product/GetProductList/" + localStorage.key(i),
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(jsonObject),
            dataType: "json",
            error: function (xhr, status, error) {
                console.log(error);
            },
            success: function (responseData) {
                console.log(responseData);
                //Display Cart Items
                var productRow = "";
               
                    productRow += "<tr>" +
                    "<td>" + responseData.ProductName + "</td>"+
                    "<td>" + responseData.ProductDescription + "</td>" +
                    "<td>" + responseData.ProductPrice + "</td>" +
                    "<td>" + localStorage.getItem(responseData.ProductModelId) + "</td>"+
                    "<td>" + localStorage.getItem(responseData.ProductModelId) * responseData.ProductPrice + "</td>" +
                    "<td><button onclick='removeItemFromCart(" + responseData.ProductModelId+")' class='btn'>Remove Item </button></td>"+
                   "</tr > ";

                $("#Cart").append(productRow);
            }
        });
    }
}

function getCheckoutParams() {
    var url = window.location.href;
    var params = {};
    var qs = url.indexOf("?") >= 0 ? url.split('?') : [];
    if (qs.length > 0) {
        var arr = qs[1].split('&');
        arr.forEach(function (entry, index) {
            var q = entry.split('=');
            if (q != null && q.length == 2) {
                params[q[0].toString().toLowerCase()] = q[1];
            }
        });
    }
    return params;
}

function removeItemFromCart(data) {
    localStorage.removeItem(data);
    location.reload();
}

function getCart(product) {
 
    return localStorage;
}