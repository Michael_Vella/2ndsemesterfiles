﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace CalculatorServiceLibary
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : ICalc
    {
        public int Add(int num, int num1)
        {
            return num + num1;
        }

        public int Subtract(int num, int num2)
        {
            return num - num2;
        }

        public int Multiply(int num, int num2)
        {
            return num * num2;
        }

        public int Division(int num, int num2)
        {
            return num / num2;
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
