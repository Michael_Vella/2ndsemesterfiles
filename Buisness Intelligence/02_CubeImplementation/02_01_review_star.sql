use vw;
go

-- review olap creation

create schema cubeReviewStar;
go

create table cubeReviewStar.[date] (
	dateKey uniqueidentifier primary key default newid(),
	yearValue numeric(4,0) not null,
	quarterValue numeric(1,0) not null,
	monthValue numeric(2,0) not null,
	dateValue date unique not null
);

create table cubeReviewStar.model (
	modelKey uniqueidentifier primary key default newid(),
	manufacturer nvarchar(256) not null,
	model nvarchar(256) not null,
	releaseYear numeric(4,0) not null,
	modelDesc nvarchar(256) not null,
	modelId uniqueidentifier unique not null,
	unique(model, releaseYear)
)

create table cubeReviewStar.client (
	clientKey uniqueidentifier primary key default newid(),
	country nvarchar(256) not null,
	region  nvarchar(256) not null,
	city  nvarchar(256) not null,
	gender char(1) not null,
	age numeric(3,0) not null,
	passportNumber nvarchar(25) not null,
	fromDate date not null,
	toDate date,
	clientDesc nvarchar(max) not null,
	clientId uniqueidentifier not null,
	unique(clientId, fromDate),
	unique(passportNumber, fromDate)
)

create table cubeReviewStar.review (
	reviewKey uniqueidentifier primary key default newid(),
	rateValue numeric(1) not null,
	rateDateKey uniqueidentifier not null references cubeReviewStar.[date](dateKey),
	modelKey uniqueidentifier not null references cubeReviewStar.model(modelKey),
	clientKey uniqueidentifier not null references cubeReviewStar.client(clientKey),
	reviewId uniqueidentifier not null unique
);

/*
	drop table cubeReviewStar.review;
	drop table cubeReviewStar.client;
	drop table cubeReviewStar.model;
	drop table cubeReviewStar.date;
	drop schema cubeReviewStar;
*/

-- ETL

insert into cubeReviewStar.[date] 
	(yearValue, quarterValue, monthValue, dateValue)
	(select distinct datepart(year,reviewDate), datepart(quarter,reviewDate), datepart(month,reviewDate),
			convert(date, cast(reviewDate as date), 111)
	 from oltp.review);

insert into cubeReviewStar.model(manufacturer, model, releaseYear, modelDesc, modelId)
(select distinct man.manufacturerName, mo.modelName, mo.releaseYear, 
	concat(man.manufacturerName,' ', mo.modelName, ' ', mo.releaseYear)
	, mo.modelId
from oltp.manufacturer man inner join oltp.model mo
	on man.manufacturerId = mo.manufacturerId
	inner join oltp.review r
	on mo.modelId = r.modelId);

insert into cubeReviewStar.client (country, region, city, gender, age, passportNumber, fromDate, toDate, clientDesc, clientId)
(select distinct 
	co.countryName, 
	re.regionName,
	ci.cityName,
	cl.gender,
	datediff(year, cl.dateOfBirth, rev.reviewDate),
	cl.passportNumber,
	convert(date,cast(rev.reviewDate as date),111),
	(select convert(date, cast(dateadd(dd,-1,min(reviewdate)) as date),111)
	from oltp.review
	where clientId = cl.clientId and reviewDate > rev.reviewDate )
	,
	concat(co.countryName , ' '  ,re.regionName , ' ' , ci.cityName , ' ' ,
		cl.gender, ' ', datediff(year, cl.dateOfBirth, rev.reviewDate), ' ', 
		cl.passportNumber,' ' ,convert(date,cast(rev.reviewDate as date),111)),
	cl.clientId
	from oltp.country co inner join oltp.region re
	on co.countryId = re.countryId
	inner join oltp.city ci
	on re.regionId = ci.regionId
	inner join oltp.client cl
	on ci.cityId = cl.cityId
	inner join oltp.review rev
	on cl.clientId = rev.clientId);

insert into cubeReviewStar.review(rateValue, rateDateKey, modelKey, clientKey, reviewId)
select rateValue,
		(select datekey from cubeReviewStar.[date] where dateValue = convert(date, cast(reviewDate as date), 111)) as 'datekey',
		(select modelkey from cubeReviewStar.model where modelid = r.modelId) as 'model',
		(select clientKey from cubeReviewStar.client where clientid = r.clientId 
		 and (
			(todate is null and r.reviewDate>=fromDate) or 
			(r.reviewDate between fromdate and todate))
		) as 'client',
		reviewId as 'oltpid'
from oltp.review r;


select mo.manufacturer, dat.yearValue, cli.gender, avg(rev.rateValue) as 'Average'
from cubeReviewStar.review rev inner join cubeReviewStar.[date] dat
	on rev.rateDateKey = dat.dateKey
	inner join cubeReviewStar.model mo
	on rev.modelKey = mo.modelKey
	inner join cubeReviewStar.client cli
	on rev.clientKey = cli.clientKey
group by rollup(mo.manufacturer, dat.yearValue, cli.gender);





select * from cubeReviewStar.[date];
select * from cubeReviewStar.model;
select * from cubeReviewStar.client;
select * from cubeReviewStar.review;