﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
namespace Lab5_WebApi.DataAccessLayer
{
    public class Brand
    {
        private string brandName;
        public string BrandName { get => brandName; set => brandName = value; }
    }
}