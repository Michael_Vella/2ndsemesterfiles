use IICT6011A01;
go

create schema olap;
go

create table olap.product (
	productKey uniqueidentifier primary key default newid(),
	category nvarchar(256) not null,
	subCategory nvarchar(256) not null,
	flavour nvarchar(256) not null,
	productDesc nvarchar(max) not null,
	productId uniqueidentifier not null unique
);

create table olap.[date] (
	dateKey uniqueidentifier primary key default newid(),
	dateYear numeric(4) not null,
	dateMonth numeric(2) not null,
	dayMonth numeric(2) not null,
	dayWeek numeric(1) not null,
	dateValue date not null unique
);

create table olap.store (
	storeKey uniqueidentifier primary key default newid(),
	[state] nvarchar(256) not null,
	town nvarchar(256) not null,
	zipCode integer not null,
	storeDesc nvarchar(max) not null,
	storeId uniqueidentifier not null unique
);

create table olap.sale (
	saleKey uniqueidentifier primary key default newid(),
	price numeric(6,2) not null,
	quantity integer not null,
	isCash bit not null,
	storeKey uniqueidentifier not null references olap.store(storeKey),
	dateKey uniqueidentifier not null references olap.[date](dateKey),
	productKey uniqueidentifier not null references olap.product(productKey)
);


insert into olap.[date] 
	(dateYear, dateMonth, dayMonth, dayWeek, dateValue)
select distinct datepart(year,receiptdate), datepart(month,receiptdate),
	datepart(day,receiptdate), DATEPART(weekday, receiptdate),
	receiptDate
from bakery.receipt;

insert into olap.store
	(state, town, zipCode, storeDesc, storeId)
select distinct stateName, townName, zipCode, 
	concat(stateName,',',townName,',',zipCode), sto.storeId
from bakery.store sto inner join bakery.town tow
	on sto.townId = tow.townId
	inner join bakery.[state] sta
	on tow.stateId = sta.stateId

insert into olap.product 
	(category, subCategory, flavour, productDesc, productId)
select distinct categoryName, subCategoryName, flavourName,
	concat(categoryName,',',subCategoryName,',',flavourName),
	pr.productId
from bakery.product pr inner join bakery.flavour fl
	on pr.flavourId = fl.flavourId
	inner join bakery.itemSubCategory isc
	on pr.subCategoryId = isc.subCategoryId
	inner join bakery.itemCategory ic
	on isc.categoryId = ic.categoryId;

insert into olap.sale
	(price, quantity, isCash, storeKey, productKey, dateKey)
select unitPrice, quantity, isCash,
	(select storeKey from olap.store where storeId = st.storeId),
	(select productKey from olap.product where productId = pr.productId),
	(select dateKey from olap.[date] where dateValue = re.receiptDate)
from bakery.receipt re inner join bakery.item it
	on re.receiptId = it.receiptId
	inner join bakery.product pr
	on it.productId = pr.productId
	inner join bakery.employee em
	on re.employeeId = em.employeeId
	inner join bakery.store st
	on st.storeId = em.storeId;

select da.dateYear, pr.category, sto.[state], sum(price*quantity) as 'total sales'
from olap.sale sa inner join olap.product pr
	on sa.productKey = pr.productKey
	inner join olap.[date] da
	on sa.dateKey = da.dateKey
	inner join olap.store sto
	on sa.storeKey = sto.storeKey
group by da.dateYear, pr.category, sto.[state];
