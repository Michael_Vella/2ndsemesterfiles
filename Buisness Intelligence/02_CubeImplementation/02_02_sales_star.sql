use vw;
go

create schema cubeSalesStar;
go

create table cubeSalesStar.[date] (
	dateKey uniqueidentifier primary key default newid(),
	yearValue numeric(4,0) not null,
	quarterValue numeric(1,0) not null,
	monthValue numeric(2,0) not null,
	dayoftheMonth numeric(2,0) not null,
	dayoftheWeek numeric(1,0) not null,
	dateValue date unique not null
);

create table cubeSalesStar.store (
	storeKey uniqueidentifier primary key default newid(),
	country nvarchar(256) not null,
	region nvarchar(256) not null,
	city nvarchar(256) not null,
	storeNumber nvarchar(256) unique not null,
	storeDesc nvarchar(max) not null,
	storeId uniqueidentifier not null
);

create table cubeSalesStar.vehicle (
	vehicleKey uniqueidentifier primary key default newid(),
	manufacturer nvarchar(256) not null,
	model nvarchar(256) not null,
	releaseYear numeric(4,0) not null,
	package nvarchar(256) not null,
	engineid nvarchar(256) unique not null,
	chassisid nvarchar(256) unique not null,
	vehicleDesc nvarchar(max) not null,
	vehicleId uniqueidentifier unique not null
)

create table cubeSalesStar.client (
	clientKey uniqueidentifier primary key default newid(),
	country nvarchar(256) not null,
	region  nvarchar(256) not null,
	city  nvarchar(256) not null,
	gender char(1) not null,
	age numeric(3,0) not null,
	passportNumber nvarchar(25) not null,
	fromDate date not null,
	toDate date,
	clientDesc nvarchar(max) not null,
	clientId uniqueidentifier not null,
	unique(clientId, fromDate),
	unique(passportNumber, fromDate)
)

create table cubeSalesStar.sale (
	saleKey uniqueidentifier primary key default newid(),
	productionCost numeric(9,2) not null,
	transportationCost numeric(9,2) not null,
	retailPrice numeric(9,2) not null,
	basePrice numeric(9,2) not null,
	taxAmount numeric(9,2) not null,
	discountAmount numeric(9,2) not null,
	dateKey uniqueidentifier not null references cubeSalesStar.[date](dateKey),
	vehicleKey uniqueidentifier not null references cubeSalesStar.vehicle(vehicleKey),
	storeKey uniqueidentifier not null references cubeSalesStar.store(storeKey),
	clientKey uniqueidentifier not null references cubeSalesStar.client(clientKey),
	invoiceNumber integer not null unique,
	saleId uniqueidentifier not null unique
);

/* clean-up 

	drop table cubeSalesStar.sale;
	drop table cubeSalesStar.client;
	drop table cubeSalesStar.[date];
	drop table cubeSalesStar.store;
	drop table cubeSalesStar.vehicle;

	drop schema cubeSalesStar;
*/


/* insert
 date
 store
 vehicle

 research: procedures tsql 
		   cursors tsql
*/