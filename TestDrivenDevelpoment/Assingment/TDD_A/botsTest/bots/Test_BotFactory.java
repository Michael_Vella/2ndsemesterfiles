package bots;


import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class Test_BotFactory {
	List<String> params = new ArrayList<String>();
	
	@Test
	public void Test_BotFactory_Fb() {
		params.add("FacebookBot");
		Bot bot = BotFactory.createBot(params);
		FacebookBot botTest = new FacebookBot(params);
		boolean isFb = bot.getClass() == botTest.getClass();
		assertTrue(isFb);
	}
	
	@Test
	public void Test_BotFactory_Weather() {
		params.add("WeatherBot");
		Bot bot = BotFactory.createBot(params);
		WeatherBot botTest = new WeatherBot(params);
		boolean isWeather = bot.getClass() == botTest.getClass();
		assertTrue(isWeather);
	}
	
	@Test
	public void Test_BotFactory_Mail() {
		params.add("MailBot");
		Bot bot = BotFactory.createBot(params);
		MailBot botTest = new MailBot(params);
		boolean isMail = bot.getClass() == botTest.getClass();
		assertTrue(isMail);
	}
	
	@Test
	public void Test_BotFactory_Empty() {
		params.add("");
		Bot bot = BotFactory.createBot(params);
		boolean isEmpty = (bot == null);
		assertTrue(isEmpty);
	}
	
	@Test
	public void Test_BotFactory_Null() {
		params.add(null);
		Bot bot = BotFactory.createBot(params);
		boolean isNull = (bot == null);
		assertTrue(isNull);
	}

}
