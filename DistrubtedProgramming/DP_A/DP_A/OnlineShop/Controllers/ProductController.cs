﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OnlineShop.Models;
using OnlineShop.CurrencyConverter;
namespace OnlineShop.Controllers
{
    public class ProductController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize]
        // GET: Product
        public ActionResult Index()
        {
            /*CurrencyModel cm = new CurrencyModel();
            return View(cm);*/
            return View();
        }

       /* [HttpPost]
        public ActionResult Index(string ddlTo, string price)
        {
            string ddlFrom = "EUR";
            ConverterSoapClient client = new ConverterSoapClient("ConverterSoap");
            decimal rate = client.GetConversionRate(ddlFrom, ddlTo, DateTime.Now);

            decimal answer = rate * Convert.ToDecimal(price);
            CurrencyModel cm = new CurrencyModel();
            cm.ConvertedValue = answer;

            return View(cm);
        }*/

        [Authorize]
        public ActionResult ViewCart()
        {
            return View();
        }
    }
}
