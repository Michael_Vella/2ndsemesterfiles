﻿using OnlineShop.CurrencyConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineShop.Models
{
        public class CurrencyModel
        {
            public IEnumerable<string> Currencies { get; set; }

            public CurrencyModel()
            {
                ConverterSoapClient myclient = new ConverterSoapClient("ConverterSoap");
                string[] abbreviations = myclient.GetCurrencies();

                Currencies = abbreviations.AsEnumerable();

            }

            public decimal ConvertedValue { get; set; }
        }
    }