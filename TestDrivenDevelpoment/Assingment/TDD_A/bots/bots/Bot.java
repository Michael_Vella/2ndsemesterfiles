package bots;
import java.util.List;

public abstract class Bot {
	
	public Bot(List<String> params) {}
	
	/**
	 * Executes the functionality of the bot
	 * @return
	 */
	public abstract boolean execute();
	
	
	/**
	 * Returns the out come of the bot execute method
	 * @return
	 */
	public abstract String getOutCome();

	/**
	 * SetOutCome is used set the out come.
	 * Takes protected string setOutCome which is to be used in the getOutCome method
	 */
	public void SetOutCome(String setOutCome) {
		this.setOutCome = setOutCome;
	}

	/**
	 * 
	 */
	protected String setOutCome;
}
