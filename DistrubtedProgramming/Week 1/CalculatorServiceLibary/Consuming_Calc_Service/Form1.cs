﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Consuming_Calc_Service.ServiceReference1;
namespace Consuming_Calc_Service
{
    public partial class Form1 : Form
    {
        CalcClient calcClient;
        private int numTwo;
        private int numOne;

        public Form1()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            this.calcClient = new CalcClient();
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            numOne = Convert.ToInt32(txt_Number1.Text);
            numTwo = Convert.ToInt32(txt_Number2.Text);
            lbl_ResultValue.Text = calcClient.Add(numOne, numTwo).ToString();
        }

        private void btn_Subtract_Click(object sender, EventArgs e)
        {
            numOne = Convert.ToInt32(txt_Number1.Text);
            numTwo = Convert.ToInt32(txt_Number2.Text);
            lbl_ResultValue.Text = calcClient.Subtract(numOne, numTwo).ToString();
        }

        private void btn_Multiply_Click(object sender, EventArgs e)
        {
            numOne = Convert.ToInt32(txt_Number1.Text);
            numTwo = Convert.ToInt32(txt_Number2.Text);
            lbl_ResultValue.Text = calcClient.Multiply(numOne, numTwo).ToString();
        }

        private void btn_Divide_Click(object sender, EventArgs e)
        {
            numOne = Convert.ToInt32(txt_Number1.Text);
            numTwo = Convert.ToInt32(txt_Number2.Text);
            lbl_ResultValue.Text = calcClient.Division(numOne, numTwo).ToString();
        }
    }
}
