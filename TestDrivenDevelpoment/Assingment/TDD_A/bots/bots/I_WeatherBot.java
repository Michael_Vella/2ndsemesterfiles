package bots;

import java.io.IOException;
import java.util.List;


public interface I_WeatherBot {
	/**
	 * Gets the weather of the request city from the country
	 */
	public String getWeather(I_WeatherBot weather, String city) throws IOException;
}
